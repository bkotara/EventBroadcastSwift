//: Playground - noun: a place where people can play

import Foundation

protocol BroadcastDataType {
    func visit<T: BroadcastDataAcceptor>(_ acceptor: T)
}

protocol BroadcastDataAcceptor: class {
    func accept(broadcastDataType: BroadcastDataType)
}

// Default accept(BroadcastDataType) calls generic visit<T: BroadcastDataAcceptor>(T), which then calls the correct accept function
extension BroadcastDataAcceptor {
    // These defaults will be private to the framework
    var acceptorId: String {
        get {
            // Could probably use some memorization here...
            return String(UInt(bitPattern: ObjectIdentifier(self)))
        }
    }
    
    func accept(broadcastDataType: BroadcastDataType) {
        broadcastDataType.visit(self)
    }
}

class Broadcaster {
    // Ain't nobody else needs to mess with this here wrapper
    private struct BroadCastDataAcceptorWrapper {
        private weak var acceptor: BroadcastDataAcceptor? // Things would be pretty weird if this wasn't weak, right?
        private var operationQueue: OperationQueue // May be weirder if this was...
        
        init(acceptor: BroadcastDataAcceptor, operationQueue: OperationQueue) {
            self.acceptor = acceptor
            self.operationQueue = operationQueue
        }
        
        func runAcceptInQueue(_ data: BroadcastDataType) {
            operationQueue.addOperation {
                self.acceptor?.accept(broadcastDataType: data)
            }
        }
    }
    
    // GlobalStation is lazily instantiated by default. No wasted resources if it isn't used.
    static let GlobalStation = Broadcaster() // Default station for use by anyone
    private lazy var operationQueue = OperationQueue()
    private lazy var acceptors = [String:BroadCastDataAcceptorWrapper]()
    
    // Uses default operation queue
    func register(acceptor: BroadcastDataAcceptor) {
        register(acceptor: acceptor, withOperationQueue: self.operationQueue)
    }
    
    // Call this directly if you want to supply your own operation queue
    func register(acceptor: BroadcastDataAcceptor, withOperationQueue queue: OperationQueue) {
        self.acceptors[acceptor.acceptorId] = BroadCastDataAcceptorWrapper(acceptor: acceptor, operationQueue: queue)
    }
    
    func unregister(acceptor: BroadcastDataAcceptor) {
        self.acceptors.removeValue(forKey: acceptor.acceptorId)
    }
    
    // Works like a megaphone
    func emit<T: BroadcastDataType>(_ data: T) {
        for (_, acceptorReference) in self.acceptors {
            acceptorReference.runAcceptInQueue(data)
        }
    }
}


/*

This is al testing code below
 
*/


struct NavigationEvent: BroadcastDataType {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func visit<T: BroadcastDataAcceptor>(_ acceptor: T) {
        (acceptor as? NavigationEventAcceptor)?.accept(navigationEvent: self)
    }
}

protocol NavigationEventAcceptor: BroadcastDataAcceptor {
    func accept(navigationEvent: NavigationEvent)
}

class Listener: NavigationEventAcceptor {
    init() {
        print("Listener Initialzed (ID: \(self.acceptorId))")
        Broadcaster.GlobalStation.register(acceptor: self, withOperationQueue: OperationQueue())
    }
    
    // Real good idea to have this guy
    deinit {
        print("Listener Deinitialized (ID: \(self.acceptorId))")
        Broadcaster.GlobalStation.unregister(acceptor: self)
    }
    
    func accept(navigationEvent: NavigationEvent) {
        print("Listener (ID: \(self.acceptorId)) recieved a navigation event: \(navigationEvent.name)")
    }
}

extension String: BroadcastDataType {
    func visit<T: BroadcastDataAcceptor>(_ acceptor: T) {
        (acceptor as? StringAcceptor)?.accept(self)
    }
}

protocol StringAcceptor: BroadcastDataAcceptor {
    func accept(_ stringData: String)
}

class ListenerSubclass: Listener, StringAcceptor {
    override func accept(navigationEvent: NavigationEvent) {
        print("ListenerSubclass (ID: \(self.acceptorId)) recieved a navigation event: \(navigationEvent.name)")
    }
    
    func accept(_ stringData: String) {
        print("ListenerSubclass (ID: \(self.acceptorId)) recieved a message: \(stringData)")
    }
}

Broadcaster.GlobalStation.emit("This ain't gettin' nowhere. No listeners yet...") // Singleton lazily instantiated

var listener1: Listener? = Listener() // Will be deinitialized when I choose
Listener() // Will be deinitialized immediately
let listenerSubclass3 = ListenerSubclass() // Will not be deinitialized

Broadcaster.GlobalStation.emit(NavigationEvent(name: "Test Nav Event One!"))
Broadcaster.GlobalStation.emit(NavigationEvent(name: "Test Nav Event Two!"))
Broadcaster.GlobalStation.emit("You get this??")

listener1 = nil // I've chosen to delete my reference (the only one) to listener1. It will be garbage collected.





